# Order picking

Analyzing data collected by me while working in order picking.

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:f9f5839f86d19e1038ae8cbd5560ad66?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:f9f5839f86d19e1038ae8cbd5560ad66?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:f9f5839f86d19e1038ae8cbd5560ad66?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/A.Iwaniuk/order-picking.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:f9f5839f86d19e1038ae8cbd5560ad66?https://gitlab.com/A.Iwaniuk/order-picking/-/settings/integrations)

## Description
I was working in order picking and decided to have a little fun analyzing data from my work. I gathered data about my hours of work, place where I'm working and average speed of my work every day.
Normal order picking is in one from 13 halls: E1, E2, E3, E4, E5, E6, K1, K2, K3, K4, K5, K6, 1OG.
There is also diffrent type of work EP and W done in all halls.
NA means that I didn't work that day.




